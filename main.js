let navbar = document.querySelector('.navbar')

let bowling = document.querySelector('.fa-bowling-ball')


document.addEventListener('scroll', () => {

    let scrolled = window.scrollY

    bowling.style.transform = `rotate(${scrolled / 2}deg)`
})